import styles from '../styles/Home.module.css';

import { useState } from 'react';
import Link from 'next/link';

export default function Nav() {
  const [open, setOpen] = useState(false);
  const [showDropdown, setShowDropdown] = useState(false);
  const [showDropdownTv, setShowDropdownTv] = useState(false);

  return (
    <nav className="relative flex items-center px-4 bg-gradient-to-r from-cyan-900 to-blue-900 py-6 md:justify-between">
      <section className="w-9/12 md:w-2/5 lg:w-1/5">
        <svg className={styles.cls1} xmlns="http://www.w3.org/2000/svg" viewBox="0 0 423.04 35.4">
          <defs>
            <linearGradient
              id="linear-gradient"
              y1="17.7"
              x2="423.04"
              y2="17.7"
              gradientUnits="userSpaceOnUse"
            >
              <stop offset="0" stop-color="#90cea1" />
              <stop offset="0.56" stop-color="#3cbec9" />
              <stop offset="1" stop-color="#00b3e5" />
            </linearGradient>
          </defs>
          <title>Asset 1</title>
          <g id="Layer_2" data-name="Layer 2">
            <g id="Layer_1-2" data-name="Layer 1">
              <path
                className="cls-1"
                d="M227.5,0h8.9l8.75,23.2h.1L254.15,0h8.35L247.9,35.4h-6.25Zm46.6,0h7.8V35.4h-7.8Zm22.2,0h24.05V7.2H304.1v6.6h15.35V21H304.1v7.2h17.15v7.2H296.3Zm55,0H363a33.54,33.54,0,0,1,8.07,1A18.55,18.55,0,0,1,377.75,4a15.1,15.1,0,0,1,4.52,5.53A18.5,18.5,0,0,1,384,17.8a16.91,16.91,0,0,1-1.63,7.58,16.37,16.37,0,0,1-4.37,5.5,19.52,19.52,0,0,1-6.35,3.37A24.59,24.59,0,0,1,364,35.4H351.29Zm7.81,28.2h4a21.57,21.57,0,0,0,5-.55,10.87,10.87,0,0,0,4-1.83,8.69,8.69,0,0,0,2.67-3.34,11.92,11.92,0,0,0,1-5.08,9.87,9.87,0,0,0-1-4.52,9,9,0,0,0-2.62-3.18,11.68,11.68,0,0,0-3.88-1.88,17.43,17.43,0,0,0-4.67-.62h-4.6ZM395.24,0h13.2a34.42,34.42,0,0,1,4.63.32,12.9,12.9,0,0,1,4.17,1.3,7.88,7.88,0,0,1,3,2.73A8.34,8.34,0,0,1,421.39,9a7.42,7.42,0,0,1-1.67,5,9.28,9.28,0,0,1-4.43,2.82v.1a10,10,0,0,1,3.18,1,8.38,8.38,0,0,1,2.45,1.85,7.79,7.79,0,0,1,1.57,2.62,9.16,9.16,0,0,1,.55,3.2,8.52,8.52,0,0,1-1.2,4.68,9.42,9.42,0,0,1-3.1,3,13.38,13.38,0,0,1-4.27,1.65,23.11,23.11,0,0,1-4.73.5h-14.5ZM403,14.15h5.65a8.16,8.16,0,0,0,1.78-.2A4.78,4.78,0,0,0,412,13.3a3.34,3.34,0,0,0,1.13-1.2,3.63,3.63,0,0,0,.42-1.8,3.22,3.22,0,0,0-.47-1.82,3.33,3.33,0,0,0-1.23-1.13,5.77,5.77,0,0,0-1.7-.58,10.79,10.79,0,0,0-1.85-.17H403Zm0,14.65h7a8.91,8.91,0,0,0,1.83-.2,4.78,4.78,0,0,0,1.67-.7,4,4,0,0,0,1.23-1.3,3.71,3.71,0,0,0,.47-2,3.13,3.13,0,0,0-.62-2A4,4,0,0,0,413,21.45,7.83,7.83,0,0,0,411,20.9a15.12,15.12,0,0,0-2.05-.15H403Zm-199,6.53H205a17.66,17.66,0,0,0,17.66-17.66h0A17.67,17.67,0,0,0,205,0h-.91A17.67,17.67,0,0,0,186.4,17.67h0A17.66,17.66,0,0,0,204.06,35.33ZM10.1,6.9H0V0H28V6.9H17.9V35.4H10.1ZM39,0h7.8V13.2H61.9V0h7.8V35.4H61.9V20.1H46.75V35.4H39ZM80.2,0h24V7.2H88v6.6h15.35V21H88v7.2h17.15v7.2h-25Zm55,0H147l8.15,23.1h.1L163.45,0H175.2V35.4h-7.8V8.25h-.1L158,35.4h-5.95l-9-27.15H143V35.4h-7.8Z"
              />
            </g>
          </g>
        </svg>
        {open && (
          <section className="bg-gradient-to-r from-cyan-900 to-blue-900 fixed inset-0 min-h-screen bg-dune bg-noise text-desert-storm overflow-auto z-50">
            <div className="w-full h-full px-4 py-6 max-w-screen-md mx-auto relative">
              <div>
                <button
                  onClick={() => setOpen(!true)}
                  type="button"
                  className="absolute top-5 right-5"
                >
                  <svg
                    className="h-8 w-8 fill-white"
                    xmlns="http://www.w3.org/2000/svg"
                    viewBox="0 0 512 512"
                  >
                    <path d="M256 8C119 8 8 119 8 256s111 248 248 248 248-111 248-248S393 8 256 8zm0 448c-110.5 0-200-89.5-200-200S145.5 56 256 56s200 89.5 200 200-89.5 200-200 200zm101.8-262.2L295.6 256l62.2 62.2c4.7 4.7 4.7 12.3 0 17l-22.6 22.6c-4.7 4.7-12.3 4.7-17 0L256 295.6l-62.2 62.2c-4.7 4.7-12.3 4.7-17 0l-22.6-22.6c-4.7-4.7-4.7-12.3 0-17l62.2-62.2-62.2-62.2c-4.7-4.7-4.7-12.3 0-17l22.6-22.6c4.7-4.7 12.3-4.7 17 0l62.2 62.2 62.2-62.2c4.7-4.7 12.3-4.7 17 0l22.6 22.6c4.7 4.7 4.7 12.3 0 17z" />
                  </svg>
                </button>
              </div>
              <div className="mt-20">
                <div className="mb-4 text-white text-base border-b-2 border-l-white">
                  <h2 className="tracking-wide leading-loose">Movies</h2>
                </div>
                <ul className="list-none space-y-3 text-white pl-6">
                  <li className="cursor-pointer">
                    <Link href="/">
                      <a className="tracking-wide leading-loose font-medium">Popular</a>
                    </Link>
                  </li>
                  <li className="cursor-pointer">
                    <Link href="/movie/now-playing">
                      <a className="tracking-wide leading-loose font-medium">Now Playing</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a className="tracking-wide leading-loose font-medium">Top Rated</a>
                    </Link>
                  </li>
                </ul>
              </div>
              <div className="mt-4">
                <div className="mb-4 text-white text-base border-b-2 border-l-white">
                  <h2 className="tracking-wide leading-loose">TV Shows</h2>
                </div>
                <ul className="list-none space-y-3 text-white pl-6">
                  <li className="cursor-pointer">
                    <Link href="">
                      <a className="tracking-wide leading-loose font-medium">Popular</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a className="tracking-wide leading-loose font-medium">Top Rated</a>
                    </Link>
                  </li>
                  <li>
                    <Link href="">
                      <a className="tracking-wide leading-loose font-medium">Airing Today</a>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </section>
        )}
      </section>
      <section className="hidden lg:w-full lg:pl-10 lg:flex lg:space-x-10">
        <div className="dropdown relative">
          <a
            className="dropdown-toggle text-white tracking-wide text-sm font-bold uppercase"
            href="#"
            type="button"
            id="dropdownMenuButton2"
            data-bs-toggle="dropdown"
            aria-expanded="false"
            onClick={() => {
              console.log(showDropdown);
              showDropdown ? setShowDropdown(false) : setShowDropdown(true);
            }}
          >
            Movies
          </a>
          <ul
            className={`dropdown-menu min-w-max absolute ${
              showDropdown ? 'block' : 'hidden'
            } bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 m-0 bg-clip-padding border-none`}
            aria-labelledby="dropdownMenuButton2"
          >
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Popular
              </a>
            </li>
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Now Playing
              </a>
            </li>
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Top Rated
              </a>
            </li>
          </ul>
        </div>
        <div className="dropdown relative">
          <a
            className="dropdown-toggle text-white tracking-wide text-sm font-bold uppercase"
            href="#"
            type="button"
            id="dropdownMenuButton3"
            data-bs-toggle="dropdown"
            aria-expanded="false"
            onClick={() => {
              console.log(showDropdownTv);
              showDropdownTv ? setShowDropdownTv(false) : setShowDropdownTv(true);
            }}
          >
            TV Shows
          </a>
          <ul
            className={`dropdown-menu min-w-max absolute ${
              showDropdownTv ? 'block' : 'hidden'
            } bg-white text-base z-50 float-left py-2 list-none text-left rounded-lg shadow-lg mt-1 m-0 bg-clip-padding border-none`}
            aria-labelledby="dropdownMenuButton3"
          >
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Popular
              </a>
            </li>
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Airing Today
              </a>
            </li>
            <li>
              <a
                className="
              dropdown-item
              text-sm
              py-2
              px-4
              font-normal
              block
              w-full
              whitespace-nowrap
              bg-transparent
              text-gray-700
              hover:bg-gray-100
            "
                href="#"
              >
                Top Rated
              </a>
            </li>
          </ul>
        </div>
      </section>
      <section className="w-1/4 flex justify-end lg:hidden">
        <button onClick={() => setOpen(true)}>
          <svg
            className="fill-slate-50 w-8 h-8"
            version="1.1"
            xmlns="http://www.w3.org/2000/svg"
            width="20"
            height="20"
            viewBox="0 0 20 20"
          >
            <title>menu</title>
            <path d="M0 3h20v2h-20v-2zM0 9h20v2h-20v-2zM0 15h20v2h-20v-2z"></path>
          </svg>
        </button>
      </section>
    </nav>
  );
}
