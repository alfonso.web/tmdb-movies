import Navbar from './navbar';
import Footer from './footer';

export default function Layout({ children }) {
  return (
    <>
      <Navbar />
      <main className="min-h-screen pt-10 px-2 pb-4 bg-zinc-50">{children}</main>
      <Footer />
    </>
  );
}
