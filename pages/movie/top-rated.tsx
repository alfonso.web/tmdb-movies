import React from 'react';
import Image from 'next/image';

const TopRated = () => {
  return (
    <section className="px-2">
      <div>
        <h1 className="font-semibold tracking-wide text-lg">Top Rated Movies</h1>
      </div>
      <section className="divide-y divide-slate-300 md:px-4 md:space-y-0 md:pt-8 md:divide-y-0 md:flex-wrap md:grid md:grid-cols-2 md:gap-4 lg:md:grid-cols-3 xl:md:grid-cols-3 xl:w-3/4 xl:mx-auto">
        <section className="py-4 flex items-center space-x-2 lg:flex-col">
          <section className="w-2/6 h-48 relative md:w-5/12 lg:w-40 lg:h-64 xl:w-48 xl:h-64">
            <Image
              src="https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg"
              alt=""
              objectFit="cover"
              layout="fill"
            />
          </section>
          <section className="w-80 lg:px-6">
            <h2 className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm">
              Doctor Strange in the Multiverse of Madness
            </h2>
            <dl>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  02/18/2022
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Fantasy, Action, Adventure
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  2h 6m
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Enter a new dimension of Strange.
                </dd>
              </div>
              <div>
                <p className="text-sm text-slate-700 leading-relaxed tracking-wide lg:text-base">
                  User score
                </p>
                <div className="w-full bg-gray-200 rounded-full">
                  <div className="bg-green-600 w-1/4 text-xs font-medium text-blue-100 text-center leading-none rounded-l-full">
                    {' '}
                    25%
                  </div>
                </div>
              </div>
            </dl>
          </section>
        </section>
        <section className="py-4 flex items-center space-x-2 lg:flex-col">
          <section className="w-2/6 h-48 relative md:w-5/12 lg:w-40 lg:h-64 xl:w-48 xl:h-64">
            <Image
              src="https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg"
              alt=""
              objectFit="cover"
              layout="fill"
            />
          </section>
          <section className="w-80 lg:px-6">
            <h2 className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm">
              Doctor Strange in the Multiverse of Madness
            </h2>
            <dl>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  02/18/2022
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Fantasy, Action, Adventure
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  2h 6m
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Enter a new dimension of Strange.
                </dd>
              </div>
              <div>
                <p className="text-sm text-slate-700 leading-relaxed tracking-wide lg:text-base">
                  User score
                </p>
                <div className="w-full bg-gray-200 rounded-full">
                  <div className="bg-green-600 w-1/4 text-xs font-medium text-blue-100 text-center leading-none rounded-l-full">
                    {' '}
                    25%
                  </div>
                </div>
              </div>
            </dl>
          </section>
        </section>
        <section className="py-4 flex items-center space-x-2 lg:flex-col">
          <section className="w-2/6 h-48 relative md:w-5/12 lg:w-40 lg:h-64 xl:w-48 xl:h-64">
            <Image
              src="https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg"
              alt=""
              objectFit="cover"
              layout="fill"
            />
          </section>
          <section className="w-80 lg:px-6">
            <h2 className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm">
              Doctor Strange in the Multiverse of Madness
            </h2>
            <dl>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  02/18/2022
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Fantasy, Action, Adventure
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  2h 6m
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Enter a new dimension of Strange.
                </dd>
              </div>
              <div>
                <p className="text-sm text-slate-700 leading-relaxed tracking-wide lg:text-base">
                  User score
                </p>
                <div className="w-full bg-gray-200 rounded-full">
                  <div className="bg-green-600 w-1/4 text-xs font-medium text-blue-100 text-center leading-none rounded-l-full">
                    {' '}
                    25%
                  </div>
                </div>
              </div>
            </dl>
          </section>
        </section>
        <section className="py-4 flex items-center space-x-2 lg:flex-col">
          <section className="w-2/6 h-48 relative md:w-5/12 lg:w-40 lg:h-64 xl:w-48 xl:h-64">
            <Image
              src="https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg"
              alt=""
              objectFit="cover"
              layout="fill"
            />
          </section>
          <section className="w-80 lg:px-6">
            <h2 className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm">
              Doctor Strange in the Multiverse of Madness
            </h2>
            <dl>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  02/18/2022
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Fantasy, Action, Adventure
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  2h 6m
                </dd>
              </div>
              <div>
                <dd className="text-sm text-slate-500 leading-relaxed tracking-wide lg:text-base">
                  Enter a new dimension of Strange.
                </dd>
              </div>
              <div>
                <p className="text-sm text-slate-700 leading-relaxed tracking-wide lg:text-base">
                  User score
                </p>
                <div className="w-full bg-gray-200 rounded-full">
                  <div className="bg-green-600 w-1/4 text-xs font-medium text-blue-100 text-center leading-none rounded-l-full">
                    {' '}
                    25%
                  </div>
                </div>
              </div>
            </dl>
          </section>
        </section>
      </section>
    </section>
  );
};

export default TopRated;
