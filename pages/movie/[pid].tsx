import { useRouter } from 'next/router';
import Image from 'next/image';

const Post = () => {
  const router = useRouter();
  const { pid } = router.query;

  return (
    <section className="flex flex-col px-2 md:px-4 lg:px-16 lg:mx-auto xl:px-72">
      <section className="flex items-center space-x-2 py-3 md:space-x-6">
        <section className="w-2/6 h-48 relative md:w-64 md:h-96 lg:h-80">
          <Image
            src="https://www.themoviedb.org/t/p/w300_and_h450_bestv2/wRnbWt44nKjsFPrqSmwYki5vZtF.jpg"
            alt=""
            objectFit="cover"
            layout="fill"
          />
        </section>
        <section className="w-80 md:space-y-3 lg:w-5/6">
          <h2 className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-lg lg:text-xl">
            Doctor Strange in the Multiverse of Madness
          </h2>
          <dl className="md:space-y-2">
            <div>
              <dd className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base lg:text-lg">
                02/18/2022
              </dd>
            </div>
            <div>
              <dd className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base lg:text-lg">
                Fantasy, Action, Adventure
              </dd>
            </div>
            <div>
              <dd className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base lg:text-lg">
                2h 6m
              </dd>
            </div>
            <div>
              <dd className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base lg:text-lg">
                Enter a new dimension of Strange.
              </dd>
            </div>
            <div>
              <p className="text-sm text-slate-700 leading-relaxed tracking-wide md:text-base lg:text-lg">
                User score
              </p>
              <div className="w-full bg-gray-200 rounded-full">
                <div className="bg-green-600 w-1/4 text-xs font-medium text-blue-100 text-center leading-none rounded-l-full">
                  {' '}
                  25%
                </div>
              </div>
            </div>
          </dl>
        </section>
      </section>
      <section className="py-3">
        <p className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
          Overview
        </p>
        <blockquote>
          <p className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Possimus id delectus sunt quae
            odio, dolorum natus alias rem debitis voluptates aspernatur dolore facilis quam, cumque
            facere, commodi ab repudiandae maiores.
          </p>
        </blockquote>
      </section>
      <section className="py-3 flex justify-end space-x-3">
        <a href="">
          <svg className="w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z" />
          </svg>
        </a>
        <a href="">
          <svg className="w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path d="M400 32H48C21.5 32 0 53.5 0 80v352c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48V80c0-26.5-21.5-48-48-48zm-48.9 158.8c.2 2.8.2 5.7.2 8.5 0 86.7-66 186.6-186.6 186.6-37.2 0-71.7-10.8-100.7-29.4 5.3.6 10.4.8 15.8.8 30.7 0 58.9-10.4 81.4-28-28.8-.6-53-19.5-61.3-45.5 10.1 1.5 19.2 1.5 29.6-1.2-30-6.1-52.5-32.5-52.5-64.4v-.8c8.7 4.9 18.9 7.9 29.6 8.3a65.447 65.447 0 0 1-29.2-54.6c0-12.2 3.2-23.4 8.9-33.1 32.3 39.8 80.8 65.8 135.2 68.6-9.3-44.5 24-80.6 64-80.6 18.9 0 35.9 7.9 47.9 20.7 14.8-2.8 29-8.3 41.6-15.8-4.9 15.2-15.2 28-28.8 36.1 13.2-1.4 26-5.1 37.8-10.2-8.9 13.1-20.1 24.7-32.9 34z" />
          </svg>
        </a>
        <a href="">
          <svg className="w-6" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512">
            <path d="M224,202.66A53.34,53.34,0,1,0,277.36,256,53.38,53.38,0,0,0,224,202.66Zm124.71-41a54,54,0,0,0-30.41-30.41c-21-8.29-71-6.43-94.3-6.43s-73.25-1.93-94.31,6.43a54,54,0,0,0-30.41,30.41c-8.28,21-6.43,71.05-6.43,94.33S91,329.26,99.32,350.33a54,54,0,0,0,30.41,30.41c21,8.29,71,6.43,94.31,6.43s73.24,1.93,94.3-6.43a54,54,0,0,0,30.41-30.41c8.35-21,6.43-71.05,6.43-94.33S357.1,182.74,348.75,161.67ZM224,338a82,82,0,1,1,82-82A81.9,81.9,0,0,1,224,338Zm85.38-148.3a19.14,19.14,0,1,1,19.13-19.14A19.1,19.1,0,0,1,309.42,189.74ZM400,32H48A48,48,0,0,0,0,80V432a48,48,0,0,0,48,48H400a48,48,0,0,0,48-48V80A48,48,0,0,0,400,32ZM382.88,322c-1.29,25.63-7.14,48.34-25.85,67s-41.4,24.63-67,25.85c-26.41,1.49-105.59,1.49-132,0-25.63-1.29-48.26-7.15-67-25.85s-24.63-41.42-25.85-67c-1.49-26.42-1.49-105.61,0-132,1.29-25.63,7.07-48.34,25.85-67s41.47-24.56,67-25.78c26.41-1.49,105.59-1.49,132,0,25.63,1.29,48.33,7.15,67,25.85s24.63,41.42,25.85,67.05C384.37,216.44,384.37,295.56,382.88,322Z" />
          </svg>
        </a>
      </section>
      <section className="py-3">
        <dl>
          <dt className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
            Status
          </dt>
          <dd className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
            Released
          </dd>
          <dt className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
            Original Language
          </dt>
          <dd className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
            English
          </dd>
          <dt className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
            Budget
          </dt>
          <dd className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
            $200,000,000.00
          </dd>
          <dt className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
            Revenue
          </dt>
          <dd className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
            $27,000,000.00
          </dd>
        </dl>
      </section>
      <section className="py-3">
        <p className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
          Recommendations
        </p>
        <section className="grid gap-2 grid-cols-2 md:grid-cols-3 lg:grid-cols-4">
          <section className="flex flex-col md:items-center">
            <div className="relative w-48 h-32">
              <Image
                src="https://www.themoviedb.org/t/p/w250_and_h141_face/5P8SmMzSNYikXpxil6BYzJ16611.jpg"
                alt=""
                objectFit="contain"
                layout="fill"
              />
            </div>
            <div>
              <p className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base">
                The Batman
              </p>
            </div>
          </section>
          <section className="flex flex-col md:items-center">
            <div className="relative w-48 h-32">
              <Image
                src="https://www.themoviedb.org/t/p/w250_and_h141_face/5P8SmMzSNYikXpxil6BYzJ16611.jpg"
                alt=""
                objectFit="contain"
                layout="fill"
              />
            </div>
            <div>
              <p className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base">
                The Batman
              </p>
            </div>
          </section>
          <section className="flex flex-col md:items-center">
            <div className="relative w-48 h-32">
              <Image
                src="https://www.themoviedb.org/t/p/w250_and_h141_face/5P8SmMzSNYikXpxil6BYzJ16611.jpg"
                alt=""
                objectFit="contain"
                layout="fill"
              />
            </div>
            <div>
              <p className="text-sm text-slate-500 leading-relaxed tracking-wide md:text-base">
                The Batman
              </p>
            </div>
          </section>
        </section>
      </section>
      <section className="py-3">
        <p className="font-semibold text-slate-900 leading-relaxed tracking-wide text-sm md:text-base lg:text-lg">
          Reviews 4
        </p>
        <section className="lg:grid lg:gap-4 lg:grid-cols-2">
          <section className="flex flex-col py-2">
            <figure className="flex flex-col-reverse">
              <blockquote className="mt-4 text-slate-700">
                <p className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
                  I was initially skeptical as I began using @tailwindcss, until I now needed to
                  copy a @sveltejs component to a different location and I didn't need to worry
                  about any of my styles breaking.
                </p>
              </blockquote>
              <figcaption className="flex items-center space-x-4">
                <div className="w-14 h-14 relative md:w-24 md:h-24">
                  <Image
                    className="rounded-full"
                    src="https://www.gravatar.com/avatar/3593437cbd05cebe0a4ee753965a8ad1.jpg"
                    alt=""
                    objectFit="contain"
                    layout="fill"
                  />
                </div>
                <div className="flex-auto">
                  <div className="text-sm text-slate-900 font-semibold md:text-base lg:text-lg">
                    <a
                      href="https://twitter.com/rotimi_best/status/1407010180760539136"
                      tabindex="0"
                    >
                      Rotimi Best
                    </a>
                  </div>
                  <div className="flex space-x-2 text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
                    <svg width="16" height="20" fill="currentColor">
                      <path d="M7.05 3.691c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.372 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.539 1.118l-2.8-2.034a1 1 0 00-1.176 0l-2.8 2.034c-.783.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.363-1.118L.98 9.483c-.784-.57-.381-1.81.587-1.81H5.03a1 1 0 00.95-.69L7.05 3.69z" />
                    </svg>
                    <span>4.8</span>
                    <span>on December 31, 2014</span>
                  </div>
                </div>
              </figcaption>
            </figure>
          </section>
          <section className="flex flex-col py-2">
            <figure className="flex flex-col-reverse">
              <blockquote className="mt-4 text-slate-700">
                <p className="text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
                  I was initially skeptical as I began using @tailwindcss, until I now needed to
                  copy a @sveltejs component to a different location and I didn't need to worry
                  about any of my styles breaking.
                </p>
              </blockquote>
              <figcaption className="flex items-center space-x-4">
                <div className="w-14 h-14 relative md:w-24 md:h-24">
                  <Image
                    className="rounded-full"
                    src="https://www.gravatar.com/avatar/3593437cbd05cebe0a4ee753965a8ad1.jpg"
                    alt=""
                    objectFit="contain"
                    layout="fill"
                  />
                </div>
                <div className="flex-auto">
                  <div className="text-sm text-slate-900 font-semibold md:text-base lg:text-lg">
                    <a
                      href="https://twitter.com/rotimi_best/status/1407010180760539136"
                      tabindex="0"
                    >
                      Rotimi Best
                    </a>
                  </div>
                  <div className="flex space-x-2 text-sm text-slate-500 font-medium leading-relaxed tracking-wide text-justify md:text-base lg:text-lg">
                    <svg width="16" height="20" fill="currentColor">
                      <path d="M7.05 3.691c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.372 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.539 1.118l-2.8-2.034a1 1 0 00-1.176 0l-2.8 2.034c-.783.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.363-1.118L.98 9.483c-.784-.57-.381-1.81.587-1.81H5.03a1 1 0 00.95-.69L7.05 3.69z" />
                    </svg>
                    <span>4.8</span>
                    <span>on December 31, 2014</span>
                  </div>
                </div>
              </figcaption>
            </figure>
          </section>
        </section>
      </section>
    </section>
  );
};

export default Post;
